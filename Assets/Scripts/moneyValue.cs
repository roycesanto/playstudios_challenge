﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class moneyValue : MonoBehaviour {

    public int value;
    public bool smallwin;
    public bool bigwin;

    void Start()
    {
        if (!smallwin && !bigwin)
        {
            value = 100;
            gameObject.GetComponent<Text>().text = value.ToString();
        }
    }

    void Update()
    {
        if (smallwin)
        {
            if (value > 0)
            { gameObject.GetComponent<Text>().text = "+" + value.ToString(); }
            else
            { gameObject.GetComponent<Text>().text = ""; }
        }
        else
        {
            if (bigwin && value == 0)
            {
                gameObject.GetComponent<Text>().text = "";
            }
            else
            {
                gameObject.GetComponent<Text>().text = value.ToString();
            }
        }
    }

    public void updateMoney(int val)
    {
        int dir = 0;
        if(val >=0)
        {
            dir = 1;
        }
        else
        {
            dir = -1;
            val = val * -1;
        }
        StartCoroutine(moneyChangeEffect(dir, val));
    }

    IEnumerator moneyChangeEffect(int dir, int val)
    {
        while(val > 0)
        {
            value += dir * 1;
            val--;
            yield return new WaitForEndOfFrame();
        }
    }

    public void emptyMoney()
    {
        StartCoroutine(moneyChangeEffect(-1, value));
    }
}
