﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class scoreScript : MonoBehaviour
{

    public GameObject[] cards;
    public GameObject money;
    public GameObject smallWinText;
    public GameObject bigWinText;
    public GameObject particleEf;
    public AudioClip flip;
    public AudioClip introclip;
    public AudioClip smallWin;
    public AudioClip bigWin;
    public AudioClip revealRest;
    public AudioClip gameoverclip;

    public int rowTotal;

    public int jackpot;
    public int cost;

    public int score = 0;
    public int cardcount = 0;
    public bool rotating = false;

    public int totalplays;
    public int highscore;
    public int totalwin;

    public bool finishfirstbool = false;
    public GameObject finishfirst;
    public GameObject gameover;
    public Text stats;
    public Text ggtxt;
    public Text scoretxt;
    public Text replaytxt;


    // Use this for initialization
    void Start()
    {
        totalplays = 0;
        highscore = 100;
        totalwin = 0;
    }

    void Update()
    {
        
    }

    public void generateNumbers()
    {
        for (int j = 0; j < 3; j++)
        {
            int temprowTotal = rowTotal;
            for (int i = 0; i < 3; i++)
            {
                int insert = 0;
                if (i == 2)
                {
                    insert = temprowTotal;
                }
                else
                {
                    insert = Random.Range(0, temprowTotal);
                }
                cards[j * 3 + i].GetComponent<cardValue>().value = insert;
                temprowTotal -= insert;
                //Debug.Log(insert);
            }
        }
    }

    public bool checkJackpot()
    {
        if (score >= jackpot)
        { return true; }
        else
        { return false; }
    }

    public void buttonClick(GameObject card)
    {
        if (card.GetComponentInChildren<Text>().text == "" && cardcount < 3 && !rotating)
        {
            StartCoroutine(RotateCard(card));
        }
    }

    IEnumerator RotateCard(GameObject card)
    {
        cardcount++;
        rotating = true;
        gameObject.GetComponent<AudioSource>().PlayOneShot(flip);
        for (int i = 0; i < 180/9; i++)
        {
            card.GetComponent<RectTransform>().Rotate(new Vector3(0, 9, 0));
            yield return new WaitForEndOfFrame();
        }


        card.GetComponentInChildren<Text>().text = card.GetComponent<cardValue>().value.ToString();
        score += card.GetComponent<cardValue>().value;
        //Debug.Log("score: " + score);

        if (cardcount >= 3)
        {
            revealAll();
            if (checkJackpot())
            {
                gameObject.GetComponent<AudioSource>().PlayOneShot(bigWin);
                bigWinText.GetComponent<moneyValue>().value = score;

                for (int i = 0; i < 15; i++)
                {
                    bigWinText.transform.localPosition -= new Vector3(0, 1000 / 15, 0);
                    yield return new WaitForEndOfFrame();
                }

                particleEf.SetActive(true);

                yield return new WaitForSeconds(1f);
                bigWinText.GetComponent<moneyValue>().updateMoney(score);
                yield return new WaitForSeconds(1f);
                money.GetComponent<moneyValue>().updateMoney(2 * score);
                bigWinText.GetComponent<moneyValue>().updateMoney(-score * 2);
                
                while(bigWinText.GetComponent<moneyValue>().value > 0)
                { yield return new WaitForEndOfFrame(); }

                totalwin += score * 2;

                particleEf.SetActive(false);
                bigWinText.transform.localPosition = new Vector3(0, 1000, 0);
            }
            else
            {
                gameObject.GetComponent<AudioSource>().PlayOneShot(smallWin);
                smallWinText.GetComponent<moneyValue>().value = score;

                yield return new WaitForSeconds(1f);
                money.GetComponent<moneyValue>().updateMoney(score);
                smallWinText.GetComponent<moneyValue>().updateMoney(-score);
                while (smallWinText.GetComponent<moneyValue>().value > 0)
                { yield return new WaitForEndOfFrame(); }

                totalwin += score;
            }

            if (highscore < money.GetComponent<moneyValue>().value)
            { highscore = money.GetComponent<moneyValue>().value; }

        }

        if(gameoverCheck())
        {
            StartCoroutine(gameoverAnim());
        }

        rotating = false;
    }

    public void revealAll()
    {
        StartCoroutine(RotateOtherCard());
    }

    IEnumerator RotateOtherCard()
    {
        rotating = true;
        gameObject.GetComponent<AudioSource>().PlayOneShot(flip);
        gameObject.GetComponent<AudioSource>().PlayOneShot(flip);
        for (int i = 0; i < 180 / 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (cards[j].GetComponentInChildren<Text>().text == "")
                {
                    cards[j].GetComponent<RectTransform>().Rotate(new Vector3(0, 9, 0));
                }
            }
            yield return new WaitForEndOfFrame();
        }
        for (int j = 0; j < 9; j++)
        {
            cards[j].GetComponentInChildren<Text>().text = cards[j].GetComponent<cardValue>().value.ToString();
        }
        rotating = false;
    }

    public bool gameoverCheck()
    {
        if (money.GetComponent<moneyValue>().value < cost * -1 && cardcount >= 3)
        { return true; }
        else
        { return false; }
    }

    public void repeat()
    {
        if (cardcount >= 3 && !rotating && !updatingMoneyCheck() && !gameoverCheck())
        {
            StartCoroutine(ResetCards());
            money.GetComponent<moneyValue>().updateMoney(cost);

            totalplays++;
            generateNumbers();
            score = 0;
            cardcount = 0;
        }
    }

    public bool updatingMoneyCheck()
    {
        if(smallWinText.GetComponent<moneyValue>().value == 0 && bigWinText.GetComponent<moneyValue>().value == 0)
        { return false; }
        else
        { return true; }
    }

    IEnumerator ResetCards()
    {
        rotating = true;
        for (int j = 0; j < 9; j++)
        {
            gameObject.GetComponent<AudioSource>().PlayOneShot(flip);
            cards[j].GetComponentInChildren<Text>().text = "";
        }
        for (int i = 0; i < 180 / 5; i++)
        {
            for (int j = 0; j < 9; j++)
            {
               cards[j].GetComponent<RectTransform>().Rotate(new Vector3(0, -5, 0));
            }
            yield return new WaitForEndOfFrame();
        }
        rotating = false;
    }

    public void restartGame()
    {
        for (int j = 0; j < 9; j++)
        {
            cards[j].GetComponentInChildren<Text>().text = "";
        }
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<AudioSource>().PlayOneShot(introclip);
        money.GetComponent<moneyValue>().value = 100;
        gameover.SetActive(false);
        cardcount = 3;
        Start();
    }

    public void quitgame()
    {
        if (cardcount >= 3 && !rotating && !updatingMoneyCheck() && !gameoverCheck())
        {
            StartCoroutine(gameoverAnim());
        }
        else
        {
            StartCoroutine(finishfirstAnim());
        }
    }

    IEnumerator gameoverAnim()
    {
        gameover.SetActive(true);
        stats.text = totalplays.ToString() + "\n" + highscore.ToString() + "\n" + totalwin.ToString();
        gameObject.GetComponent<AudioSource>().PlayOneShot(gameoverclip);

        gameover.GetComponent<Image>().CrossFadeAlpha(0f, 0f, true);
        stats.CrossFadeAlpha(0f, 0f, true);
        ggtxt.CrossFadeAlpha(0f, 0f, true);
        scoretxt.CrossFadeAlpha(0f, 0f, true);
        replaytxt.CrossFadeAlpha(0f, 0f, true);
        yield return new WaitForEndOfFrame();

        gameover.GetComponent<Image>().CrossFadeAlpha(1f, 1f, true);
        yield return new WaitForSeconds(1f);

        stats.CrossFadeAlpha(1f, 1f, true);
        ggtxt.CrossFadeAlpha(1f, 1f, true);
        scoretxt.CrossFadeAlpha(1f, 1f, true);
        yield return new WaitForSeconds(1f);

        replaytxt.CrossFadeAlpha(1f, 1f, true);
        yield return new WaitForSeconds(1f);
    }

    IEnumerator finishfirstAnim()
    {
        if (!finishfirstbool)
        {
            finishfirstbool = true;
            finishfirst.SetActive(true);
            finishfirst.GetComponent<Text>().CrossFadeAlpha(1f, 0f, true);
            yield return new WaitForSeconds(1f);

            finishfirst.GetComponent<Text>().CrossFadeAlpha(0f, 0.5f, true);
            yield return new WaitForSeconds(0.5f);
            finishfirstbool = false;
        }
    }
}
