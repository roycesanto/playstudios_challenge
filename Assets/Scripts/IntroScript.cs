﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroScript : MonoBehaviour {

    public GameObject introParent;
    public Image circle;
    public Text circleTxt;
    public Text title;

    public AudioClip introbg;

    public bool tutorial;
    public GameObject blackscreen;
    public GameObject repeat;
    public GameObject quit;
    public GameObject Text1;
    public GameObject Text2;
    public GameObject Text3;
    public GameObject Text4;
    public GameObject Text5;


    public void introAnim()
    {
        gameObject.GetComponent<AudioSource>().PlayOneShot(introbg);
        StartCoroutine(introFade());
    }
    
    IEnumerator introFade()
    {
        introParent.GetComponent<Image>().CrossFadeAlpha(0f, 1f, true);
        circle.CrossFadeAlpha(0f, 1f, true);
        circleTxt.CrossFadeAlpha(0f, 1f, true);
        title.CrossFadeAlpha(0f, 1f, true);
        yield return new WaitForSeconds(1f);

        introParent.SetActive(false);
        if (!PlayerPrefs.HasKey("Tutorial"))
        {
            tutorial = true;
            blackscreen.SetActive(true);
            PlayerPrefs.SetInt("Tutorial", 1);
        }
        else
        {
            tutorial = false;
            blackscreen.SetActive(false);
        }
    }

    public void starClick()
    {
        if(tutorial)
        {
            Text1.SetActive(false);
            Text2.SetActive(true);
            repeat.transform.SetSiblingIndex(2);
        }
    }

    public void tutorialSequence()
    {
        if (Text2.activeSelf)
        {
            Text2.SetActive(false);
            Text3.SetActive(true);
        }
        else if (Text3.activeSelf)
        {
            quit.transform.SetAsLastSibling();
            Text3.SetActive(false);
            Text4.SetActive(true);
        }
        else if (Text4.activeSelf)
        {
            Text4.SetActive(false);
            Text5.SetActive(true);
            quit.transform.SetSiblingIndex(3);
        }
        else
        {             
            Text5.SetActive(false);
            blackscreen.SetActive(false);
        }
    }

}
